'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

//get all defined properties (filters out null, undefined, ...)
exports.default = function (object) {
  return object && Object.keys(object).reduce(function (defined, key) {
    var value = object[key];
    if (value === false || value === 0 || value && value !== '') {
      defined[key] = value;
    }
    return defined;
  }, {});
};