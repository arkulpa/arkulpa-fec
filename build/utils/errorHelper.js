'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

var MISSING_FIELD = /Variable "\$(.*)" of required type .* was not provided\./;
var MISSING_FIELD_MONGOOSE = /.*validation failed: .*: Path `(.*)` is required\./;

var MissingInputError = exports.MissingInputError = { message: 'Nicht alle notwendigen Informationen wurden ausgefüllt.' };
var DetailMissingInputError = exports.DetailMissingInputError = function DetailMissingInputError(field) {
  return {
    message: 'Nicht alle notwendigen Informationen wurden ausgefüllt.' + (field ? ' (' + field + ')' : '')
  };
};

exports.default = function (_ref) {
  var graphQLErrors = _ref.graphQLErrors,
      networkError = _ref.networkError;

  var errorMessages = [];
  if (graphQLErrors) {
    errorMessages = graphQLErrors.reduce(function (result, _ref2, i, _ref3) {
      var message = _ref2.message,
          rest = _objectWithoutProperties(_ref2, ['message']);

      var length = _ref3.length;

      var missingFields = MISSING_FIELD.exec(message);
      if (!missingFields) {
        missingFields = MISSING_FIELD_MONGOOSE.exec(message);
      }
      if (missingFields) {
        var field = convertMissingField(missingFields[1]);
        result.push(DetailMissingInputError(field));
      } else {
        result.push({ message: message });
      }
      return result;
    }, errorMessages);
  }

  if (networkError) {
    errorMessages.push({ message: 'Leider ist ein Problem mit der Verbindung aufgetreten.' });
  }

  return errorMessages;
};

function convertMissingField(field) {
  switch (field) {
    case 'email':
      return 'Email';
    case 'phone':
      return 'Telefonnummer';
    case 'firstName':
      return 'Vorname';
    case 'lastName':
      return 'Nachname';
    case 'birthdayStr':
      return 'Geburtstag';
    case 'street':
      return 'Straße';
    case 'zipCode':
      return 'PLZ';
    case 'city':
      return 'Wohnort';
    case 'svNumber':
      return 'SV Nummer';
    case 'passportNumber':
      return 'Passnummer';
    case 'passportAgency':
      return 'Ausstellungsbehörde';
    case 'education':
      return 'Schulbildung';
    case 'job':
      return 'Beruf';
    case 'name':
      return 'Name';
    case 'locale':
      return 'Locale';
    case 'startDateStr':
      return 'Startzeitpunkt';
    case 'endDateStr':
      return 'Endzeitpunkt';
    case 'meetingPoint':
      return 'Treffpunkt';
    case 'password':
      return 'Passwort';
    case 'nationality':
      return 'Staatsbürgerschaft';
    case 'subject':
      return 'Betreff';
    case 'text':
      return 'Nachricht';
    default:
      return field;
  }
}