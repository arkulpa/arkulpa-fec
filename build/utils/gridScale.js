'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _polished = require('polished');

function gridScale(props, scale) {
  var baseGrid = props.theme.dimensions.baseGrid;

  return (0, _polished.rem)(baseGrid * scale);
}

exports.default = gridScale;