'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _templateObject = _taggedTemplateLiteral(['\n  padding: 20px;\n  margin: 0 -15px 20px -15px;\n  text-align: center;\n  background: lightgrey;\n'], ['\n  padding: 20px;\n  margin: 0 -15px 20px -15px;\n  text-align: center;\n  background: lightgrey;\n']),
    _templateObject2 = _taggedTemplateLiteral(['\n  margin: 20px -15px 0px;\n  padding: 10px 20px;\n'], ['\n  margin: 20px -15px 0px;\n  padding: 10px 20px;\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _Dialog = require('material-ui/Dialog');

var _Dialog2 = _interopRequireDefault(_Dialog);

var _FlatButton = require('material-ui/FlatButton');

var _FlatButton2 = _interopRequireDefault(_FlatButton);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _TextField = require('material-ui/TextField');

var _TextField2 = _interopRequireDefault(_TextField);

var _ActionButtonRow = require('../ActionButtonRow');

var _ActionButtonRow2 = _interopRequireDefault(_ActionButtonRow);

var _Container = require('../Container');

var _FormElements = require('../FormElements');

var _ErrorMessage = require('../ErrorMessage');

var _ErrorMessage2 = _interopRequireDefault(_ErrorMessage);

var _errorHelper = require('../../utils/errorHelper');

var _errorHelper2 = _interopRequireDefault(_errorHelper);

var _filterOutNotDefinedValues = require('../../utils/filterOutNotDefinedValues');

var _filterOutNotDefinedValues2 = _interopRequireDefault(_filterOutNotDefinedValues);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var LogoContainer = _styledComponents2.default.div(_templateObject);

var StyledActionButtonRow = (0, _styledComponents2.default)(_ActionButtonRow2.default)(_templateObject2);

var Login = function (_Component) {
  _inherits(Login, _Component);

  function Login() {
    _classCallCheck(this, Login);

    var _this = _possibleConstructorReturn(this, (Login.__proto__ || Object.getPrototypeOf(Login)).call(this));

    _this.login = function () {
      return _this.props.login((0, _filterOutNotDefinedValues2.default)(_this.state)).then(function () {
        _this.props.rerender();
        _this.props.history.replace('/');
      }).catch(function (rawErrors) {
        var errors = (0, _errorHelper2.default)(rawErrors);
        _this.setState({ errors: errors });
        console.log('there was an error sending the query', errors);
      });
    };

    _this._renderForgotPasswordSuccessDialog = function () {
      return _react2.default.createElement(
        _Dialog2.default,
        {
          actions: [_react2.default.createElement(_FlatButton2.default, {
            label: 'Ok',
            onTouchTap: function onTouchTap() {
              return _this.setState({ showForgotPasswordSuccessDialog: false });
            },
            primary: true
          })],
          modal: true,
          open: _this.state.showForgotPasswordSuccessDialog,
          title: 'Email f\xFCr das Zur\xFCcksetzen versendet'
        },
        _react2.default.createElement(
          'div',
          null,
          'Ihre Anfrage, um das Passwort zu \xE4ndern, wurde versendet.',
          _react2.default.createElement('br', null),
          'Sie erhalten bald eine E-Mail mit dem ben\xF6tigten Link.'
        )
      );
    };

    _this.state = { email: '', password: '', forgotPasswordSending: false, showForgotPasswordSuccessDialog: false };
    return _this;
  }

  _createClass(Login, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props$options = this.props.options,
          logo = _props$options.logo,
          showRegister = _props$options.showRegister,
          showForgotPassword = _props$options.showForgotPassword;

      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(_ErrorMessage2.default, {
          clearErrors: function clearErrors() {
            return _this2.setState({ errors: [] });
          },
          errors: this.state.errors
        }),
        _react2.default.createElement(
          _Container.Centered500,
          null,
          _react2.default.createElement(
            LogoContainer,
            null,
            logo && _react2.default.createElement('img', {
              alt: 'Logo',
              src: logo,
              width: 128
            })
          ),
          _react2.default.createElement(
            _FormElements.FormRow,
            null,
            _react2.default.createElement(_TextField2.default, {
              autoFocus: true,
              fullWidth: true,
              hintText: 'Email',
              onChange: function onChange(event) {
                _this2.setState({ email: event.target.value });
              },
              value: this.state.email
            })
          ),
          _react2.default.createElement(
            _FormElements.FormRow,
            null,
            _react2.default.createElement(_TextField2.default, {
              fullWidth: true,
              hintText: 'Passwort',
              onChange: function onChange(event) {
                _this2.setState({ password: event.target.value });
              },
              onKeyPress: function onKeyPress(target) {
                if (target.charCode === 13) {
                  _this2.login();
                }
              },
              type: 'password',
              value: this.state.password
            })
          ),
          _react2.default.createElement(
            StyledActionButtonRow,
            null,
            _react2.default.createElement('div', null),
            _react2.default.createElement(
              'div',
              null,
              showForgotPassword && _react2.default.createElement(_FlatButton2.default, {
                disabled: this.state.forgotPasswordSending,
                label: 'passwort vergessen',
                onClick: function onClick() {
                  var _filterOutNotDefinedV = (0, _filterOutNotDefinedValues2.default)(_this2.state),
                      email = _filterOutNotDefinedV.email;

                  _this2.setState({ forgotPasswordSending: true });
                  _this2.props.forgotPassword({ variables: { email: email } }).then(function (_ref) {
                    var success = _ref.data.forgotPassword;
                    return _this2.setState({ showForgotPasswordSuccessDialog: success, forgotPasswordSending: false });
                  }).catch(function (rawErrors) {
                    _this2.setState({ forgotPasswordSending: false });
                    var errors = (0, _errorHelper2.default)(rawErrors);
                    _this2.setState({ errors: errors });
                    console.log('there was an error sending the forgot password mutation', errors);
                  });
                }
              }),
              showRegister && _react2.default.createElement(_FlatButton2.default, {
                label: 'registrieren',
                onClick: function onClick() {
                  return _this2.props.history.push('/register');
                }
              }),
              _react2.default.createElement(_FlatButton2.default, {
                label: 'anmelden',
                onClick: this.login,
                primary: true
              })
            )
          )
        ),
        showForgotPassword && this._renderForgotPasswordSuccessDialog()
      );
    }
  }]);

  return Login;
}(_react.Component);

Login.defaultProps = {
  options: {}
};

exports.default = Login;