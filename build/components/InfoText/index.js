'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.InfoText = exports.INFO_SUCCESS = exports.INFO_ERROR = exports.INFO_WARNING = undefined;

var _templateObject = _taggedTemplateLiteral(['\n  font-size: 20px;\n  margin: 20px 0;\n  padding: 20px 15px;\n  font-weight: light;\n  background: ', ';\n'], ['\n  font-size: 20px;\n  margin: 20px 0;\n  padding: 20px 15px;\n  font-weight: light;\n  background: ', ';\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var INFO_WARNING = exports.INFO_WARNING = '#FEEFB3';
var INFO_ERROR = exports.INFO_ERROR = '#FFD2D2';
var INFO_SUCCESS = exports.INFO_SUCCESS = '#DFF2BF';

var InfoText = exports.InfoText = _styledComponents2.default.div(_templateObject, function (props) {
  return props.type ? props.type : INFO_WARNING;
});