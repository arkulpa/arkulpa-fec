'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _templateObject = _taggedTemplateLiteral(['\n  border-top: 1px solid lightgrey;\n  margin: 60px -40px 0;\n  padding: 10px 20px 0;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n'], ['\n  border-top: 1px solid lightgrey;\n  margin: 60px -40px 0;\n  padding: 10px 20px 0;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var ActionButtonRow = _styledComponents2.default.div(_templateObject);
exports.default = ActionButtonRow;