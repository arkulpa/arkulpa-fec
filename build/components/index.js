'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; // this is the primary export for Blocks
// All components are exported from lib/index.js


var _ActionButtonRow = require('./ActionButtonRow');

var _ActionButtonRow2 = _interopRequireDefault(_ActionButtonRow);

var _AuthRoute = require('./AuthRoute');

var _AuthRoute2 = _interopRequireDefault(_AuthRoute);

var _Container = require('./Container');

var Container = _interopRequireWildcard(_Container);

var _DeleteModal = require('./DeleteModal');

var _DeleteModal2 = _interopRequireDefault(_DeleteModal);

var _EntityTable = require('./EntityTable');

var _EntityTable2 = _interopRequireDefault(_EntityTable);

var _ErrorMessage = require('./ErrorMessage');

var _ErrorMessage2 = _interopRequireDefault(_ErrorMessage);

var _FormElements = require('./FormElements');

var FormElements = _interopRequireWildcard(_FormElements);

var _FullscreenWrapper = require('./FullscreenWrapper');

var _FullscreenWrapper2 = _interopRequireDefault(_FullscreenWrapper);

var _InfoText = require('./InfoText');

var InfoText = _interopRequireWildcard(_InfoText);

var _LoadingIndicator = require('./LoadingIndicator');

var _LoadingIndicator2 = _interopRequireDefault(_LoadingIndicator);

var _Login = require('./Login');

var _Login2 = _interopRequireDefault(_Login);

var _Select = require('./Select');

var _Select2 = _interopRequireDefault(_Select);

var _Selector = require('./Selector');

var _Selector2 = _interopRequireDefault(_Selector);

var _Sidebar = require('./Sidebar');

var Sidebar = _interopRequireWildcard(_Sidebar);

var _SidebarToggle = require('./SidebarToggle');

var _SidebarToggle2 = _interopRequireDefault(_SidebarToggle);

var _Text = require('./Text');

var Text = _interopRequireWildcard(_Text);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = _extends({
  // Blocks will be exported here
  ActionButtonRow: _ActionButtonRow2.default,
  ErrorMessage: _ErrorMessage2.default,
  Login: _Login2.default
}, Container, FormElements, {
  SidebarToggle: _SidebarToggle2.default,
  AuthRoute: _AuthRoute2.default
}, Text, {
  EntityTable: _EntityTable2.default,
  LoadingIndicator: _LoadingIndicator2.default
}, Sidebar, {
  Select: _Select2.default,
  Selector: _Selector2.default,
  DeleteModal: _DeleteModal2.default
}, InfoText, {
  FullScreenWrapper: _FullscreenWrapper2.default
});