'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _templateObject = _taggedTemplateLiteral(['\n  text-align: center;\n  margin-bottom: 20px;\n'], ['\n  text-align: center;\n  margin-bottom: 20px;\n']);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _CircularProgress = require('material-ui/CircularProgress');

var _CircularProgress2 = _interopRequireDefault(_CircularProgress);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Wrapper = _styledComponents2.default.div(_templateObject);

var LoadingIndicator = function LoadingIndicator(_ref) {
  var size = _ref.size;
  return _react2.default.createElement(
    Wrapper,
    null,
    _react2.default.createElement(_CircularProgress2.default, size)
  );
};

LoadingIndicator.defaultProps = {
  size: 80
};

exports.default = LoadingIndicator;