'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _templateObject = _taggedTemplateLiteral(['\n  position: absolute;\n  left: 20px;\n  top: 20px;\n  width: 40px;\n  height: 40px;\n  background: ', ';\n  padding: 3px 3px 3px 6px;\n  text-align: center;\n  color: ', ';\n  cursor: pointer;\n  z-index: 500;\n  overflow: hidden;\n'], ['\n  position: absolute;\n  left: 20px;\n  top: 20px;\n  width: 40px;\n  height: 40px;\n  background: ', ';\n  padding: 3px 3px 3px 6px;\n  text-align: center;\n  color: ', ';\n  cursor: pointer;\n  z-index: 500;\n  overflow: hidden;\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var SidebarToggle = _styledComponents2.default.div(_templateObject, function (props) {
  return props.theme.primary1Color;
}, function (props) {
  return props.theme.accent1Color;
});
exports.default = SidebarToggle;