'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Sidebar = exports.SidebarToggle = exports.SidebarClose = exports.SidebarWrapper = exports.SidebarNexGroupNavItem = exports.SidebarLogout = exports.SidebarNaviItem = exports.sidebarStyles = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _templateObject = _taggedTemplateLiteral(['\n  padding: 61px;\n  position: absolute;\n  top: 0;\n  left: 0;\n  right: 0;\n  height: 160px;\n'], ['\n  padding: 61px;\n  position: absolute;\n  top: 0;\n  left: 0;\n  right: 0;\n  height: 160px;\n']),
    _templateObject2 = _taggedTemplateLiteral(['\n  line-height: 50px;\n  font-size: 25px;\n  font-weight: 200;\n  cursor: pointer;\n  a,\n  a:hover {\n    color: ', ';\n    text-decoration: none;\n  }\n  a:active,\n  a:focus {\n    color: ', ';\n  }\n'], ['\n  line-height: 50px;\n  font-size: 25px;\n  font-weight: 200;\n  cursor: pointer;\n  a,\n  a:hover {\n    color: ', ';\n    text-decoration: none;\n  }\n  a:active,\n  a:focus {\n    color: ', ';\n  }\n']),
    _templateObject3 = _taggedTemplateLiteral(['\n  margin-top: 80px;\n  color: ', ';\n'], ['\n  margin-top: 80px;\n  color: ', ';\n']),
    _templateObject4 = _taggedTemplateLiteral(['\n  margin-top: 60px;\n'], ['\n  margin-top: 60px;\n']),
    _templateObject5 = _taggedTemplateLiteral(['\n  padding-top: 150px;\n'], ['\n  padding-top: 150px;\n']),
    _templateObject6 = _taggedTemplateLiteral(['\n  position: absolute;\n  bottom: 10px;\n  left: 10px;\n  text-align: center;\n  right: 10px;\n  font-size: 14px;\n  color: white;\n  cursor: pointer;\n'], ['\n  position: absolute;\n  bottom: 10px;\n  left: 10px;\n  text-align: center;\n  right: 10px;\n  font-size: 14px;\n  color: white;\n  cursor: pointer;\n']),
    _templateObject7 = _taggedTemplateLiteral(['\n  position: absolute;\n  left: 20px;\n  top: 20px;\n  width: 40px;\n  height: 40px;\n  background:  ', ';\n  padding: 3px 3px 3px 6px;\n  text-align: center;\n  color: ', ';\n  cursor: pointer;\n  z-index: 500;\n  overflow: hidden;\n'], ['\n  position: absolute;\n  left: 20px;\n  top: 20px;\n  width: 40px;\n  height: 40px;\n  background:  ', ';\n  padding: 3px 3px 3px 6px;\n  text-align: center;\n  color: ', ';\n  cursor: pointer;\n  z-index: 500;\n  overflow: hidden;\n']);

var _reactSidebar = require('react-sidebar');

var _reactSidebar2 = _interopRequireDefault(_reactSidebar);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var LogoWrapper = _styledComponents2.default.div(_templateObject);

var sidebarStyles = exports.sidebarStyles = {
  root: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    overflow: 'hidden'
  },
  sidebar: {
    zIndex: 2500,
    position: 'absolute',
    top: 0,
    bottom: 0,
    transition: 'transform .3s ease-out',
    WebkitTransition: '-webkit-transform .3s ease-out',
    willChange: 'transform',
    overflowY: 'auto',
    background: '#454849',
    padding: 40,
    minWidth: 250
  },
  content: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    overflowY: 'scroll',
    WebkitOverflowScrolling: 'touch',
    transition: 'left .3s ease-out, right .3s ease-out'
  },
  overlay: {
    zIndex: 2,
    position: 'fixed',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    opacity: 0,
    visibility: 'hidden',
    transition: 'opacity .3s ease-out, visibility .3s ease-out',
    backgroundColor: 'rgba(0,0,0,.3)'
  },
  dragHandle: {
    zIndex: 1,
    position: 'fixed',
    top: 0,
    bottom: 0
  }
};

var SidebarNaviItem = exports.SidebarNaviItem = _styledComponents2.default.div(_templateObject2, function (props) {
  return props.theme.menuColor;
}, function (props) {
  return props.theme.menuColor;
});

var SidebarLogout = exports.SidebarLogout = (0, _styledComponents2.default)(SidebarNaviItem)(_templateObject3, function (props) {
  return props.theme.menuColor;
});

var SidebarNexGroupNavItem = exports.SidebarNexGroupNavItem = (0, _styledComponents2.default)(SidebarNaviItem)(_templateObject4);

var SidebarWrapper = exports.SidebarWrapper = _styledComponents2.default.div(_templateObject5);

var SidebarClose = exports.SidebarClose = _styledComponents2.default.div(_templateObject6);

var SidebarToggle = exports.SidebarToggle = _styledComponents2.default.div(_templateObject7, function (props) {
  return props.theme.primary1Color;
}, function (props) {
  return props.theme.accent1Color;
});

var Sidebar = exports.Sidebar = function (_React$Component) {
  _inherits(Sidebar, _React$Component);

  function Sidebar(props) {
    _classCallCheck(this, Sidebar);

    var _this = _possibleConstructorReturn(this, (Sidebar.__proto__ || Object.getPrototypeOf(Sidebar)).call(this, props));

    _this.handleOpenSidebar = function (open) {
      _this.setState({ sidebarOpen: open });
      if (open) {
        document.addEventListener('keydown', _this.handleESC, false);
      } else {
        document.removeEventListener('keydown', _this.handleESC, false);
      }
    };

    _this.handleESC = function (event) {
      if (event.keyCode === 27) {
        _this.handleOpenSidebar(false);
      }
    };

    _this.state = { sidebarOpen: false };
    return _this;
  }

  _createClass(Sidebar, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          children = _props.children,
          logo = _props.logo,
          sidebarContent = _props.sidebarContent;

      return _react2.default.createElement(
        _reactSidebar2.default,
        {
          onSetOpen: this.handleOpenSidebar,
          open: this.state.sidebarOpen,
          sidebar: _react2.default.createElement(
            'div',
            null,
            _react2.default.createElement(
              LogoWrapper,
              null,
              _react2.default.createElement('img', {
                alt: 'Logo',
                src: logo,
                width: '128'
              })
            ),
            _react2.default.createElement(
              'div',
              { onClick: function onClick() {
                  return _this2.handleOpenSidebar(false);
                } },
              sidebarContent
            ),
            _react2.default.createElement(
              SidebarClose,
              { onClick: function onClick() {
                  return _this2.handleOpenSidebar(false);
                } },
              'close (ESC)'
            )
          ),
          styles: sidebarStyles
        },
        _react2.default.createElement(
          SidebarToggle,
          { onClick: function onClick() {
              return _this2.handleOpenSidebar(true);
            } },
          _react2.default.createElement('img', {
            alt: 'Logo',
            height: '34',
            src: logo,
            width: '120'
          })
        ),
        children
      );
    }
  }]);

  return Sidebar;
}(_react2.default.Component);

Sidebar.propTypes = {
  children: _propTypes2.default.node.isRequired,
  logo: _propTypes2.default.string.isRequired,
  sidebarContent: _propTypes2.default.node.isRequired
};