'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.HeadlineH2 = exports.HeadlineH1 = undefined;

var _templateObject = _taggedTemplateLiteral(['\n  font-size: 52px;\n  font-weight: lighter;\n  margin: 0.6em 0 0.1em;\n'], ['\n  font-size: 52px;\n  font-weight: lighter;\n  margin: 0.6em 0 0.1em;\n']),
    _templateObject2 = _taggedTemplateLiteral(['\n  font-size: 25px;\n  font-weight: 200;\n  margin: ', ';\n'], ['\n  font-size: 25px;\n  font-weight: 200;\n  margin: ', ';\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var HeadlineH1 = exports.HeadlineH1 = _styledComponents2.default.h1(_templateObject);
var HeadlineH2 = exports.HeadlineH2 = _styledComponents2.default.h2(_templateObject2, function (props) {
  return props.margin ? props.margin : '2em 0 0.5em';
});