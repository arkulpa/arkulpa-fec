'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Centered500 = undefined;

var _templateObject = _taggedTemplateLiteral(['\n  margin: 100px auto;\n  width: 500px;\n  background: white;\n  max-width: 100%;\n  padding: 0 15px;\n'], ['\n  margin: 100px auto;\n  width: 500px;\n  background: white;\n  max-width: 100%;\n  padding: 0 15px;\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Centered500 = exports.Centered500 = _styledComponents2.default.div(_templateObject);
exports.default = Centered500;