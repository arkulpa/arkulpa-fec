'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Centered1000 = exports.Centered500 = exports.Centered300 = undefined;

var _templateObject = _taggedTemplateLiteral(['\n  margin: 100px auto;\n  width: 300px;\n  background: rgba(255, 255, 255, 1);\n'], ['\n  margin: 100px auto;\n  width: 300px;\n  background: rgba(255, 255, 255, 1);\n']),
    _templateObject2 = _taggedTemplateLiteral(['\n  margin: 100px auto;\n  width: 500px;\n  background: white;\n  max-width: 100%;\n  padding: 0 15px;\n'], ['\n  margin: 100px auto;\n  width: 500px;\n  background: white;\n  max-width: 100%;\n  padding: 0 15px;\n']),
    _templateObject3 = _taggedTemplateLiteral(['\n  position: relative;\n  margin: 40px auto;\n  padding: 10px 40px;\n  min-width: 1000px;\n  max-width: 1400px;\n  background: rgba(255, 255, 255, 1);\n  min-height: 80%;\n'], ['\n  position: relative;\n  margin: 40px auto;\n  padding: 10px 40px;\n  min-width: 1000px;\n  max-width: 1400px;\n  background: rgba(255, 255, 255, 1);\n  min-height: 80%;\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Centered300 = exports.Centered300 = _styledComponents2.default.div(_templateObject);

var Centered500 = exports.Centered500 = _styledComponents2.default.div(_templateObject2);

var Centered1000 = exports.Centered1000 = _styledComponents2.default.div(_templateObject3);