'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Select = exports.Selector = undefined;

var _templateObject = _taggedTemplateLiteral(['\n  display: inline-block;\n  border-radius: 5px;\n  padding: 0px 10px;\n  margin: 0 10px 10px 0;\n  line-height: 30px;\n  background-color: ', ';\n  color: ', ';\n  cursor: pointer;\n'], ['\n  display: inline-block;\n  border-radius: 5px;\n  padding: 0px 10px;\n  margin: 0 10px 10px 0;\n  line-height: 30px;\n  background-color: ', ';\n  color: ', ';\n  cursor: pointer;\n']),
    _templateObject2 = _taggedTemplateLiteral(['\n  margin-bottom: -10px;\n'], ['\n  margin-bottom: -10px;\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _materialUi = require('material-ui');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Option = _styledComponents2.default.div(_templateObject, function (_ref) {
  var selected = _ref.selected,
      theme = _ref.theme;
  return selected ? theme.primary1Color : theme.accent1Color;
}, function (_ref2) {
  var selected = _ref2.selected;
  return selected ? 'white' : 'black';
});

var Wrapper = _styledComponents2.default.div(_templateObject2);

var _isSelected = function _isSelected(item, selected) {
  return selected.findIndex(function (_ref3) {
    var _id = _ref3._id;
    return _id === item._id;
  }) !== -1;
};

var _toggle = function _toggle(item, selected, updateMethod, multiSelect) {
  if (!multiSelect) {
    updateMethod([item]);
    return;
  }
  var filtered = selected.filter(function (_ref4) {
    var _id = _ref4._id;
    return _id !== item._id;
  });
  if (filtered.length === selected.length) {
    filtered.push(item);
  }
  updateMethod(filtered);
};

var Selector = exports.Selector = function Selector(_ref5) {
  var selectedItems = _ref5.selectedItems,
      items = _ref5.items,
      changeMethod = _ref5.changeMethod,
      multiselect = _ref5.multiselect,
      renderItem = _ref5.renderItem;

  return _react2.default.createElement(
    Wrapper,
    null,
    items.map(function (item) {
      return _react2.default.createElement(
        Option,
        {
          key: item._id,
          onClick: function onClick() {
            return _toggle(item, selectedItems, changeMethod, multiselect);
          },
          selected: _isSelected(item, selectedItems)
        },
        renderItem(item)
      );
    })
  );
};

Selector.propTypes = {
  changeMethod: _propTypes2.default.func.isRequired,
  items: _propTypes2.default.array.isRequired,
  multiselect: _propTypes2.default.bool,
  renderItem: _propTypes2.default.func.isRequired,
  selectedItems: _propTypes2.default.array.isArray
};

var Select = exports.Select = function Select(_ref6) {
  var label = _ref6.label,
      selectedItem = _ref6.selectedItem,
      items = _ref6.items,
      changeMethod = _ref6.changeMethod;
  return _react2.default.createElement(
    _materialUi.SelectField,
    {
      floatingLabelText: label,
      fullWidth: true,
      onChange: function onChange(_, __, value) {
        return changeMethod(value);
      },
      value: selectedItem
    },
    items.map(function (item, idx) {
      return _react2.default.createElement(_materialUi.MenuItem, {
        key: idx,
        primaryText: item.name,
        value: item._id
      });
    })
  );
};

Select.propTypes = {
  changeMethod: _propTypes2.default.func.isRequired,
  items: _propTypes2.default.array.isRequired,
  label: _propTypes2.default.string.isRequired,
  selectedItem: _propTypes2.default.object
};