'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Label = exports.Link = exports.NewButton = exports.CheckBoxStyles = exports.FormRow = undefined;

var _templateObject = _taggedTemplateLiteral(['\n  padding: 10px 15px;\n'], ['\n  padding: 10px 15px;\n']),
    _templateObject2 = _taggedTemplateLiteral(['\n  position: absolute;\n  padding: 0 10px;\n  right: 100px;\n  top: 0px;\n  height: 40px;\n  font-size: 25px;\n  font-weight: 200;\n  lineheight: 40px;\n  background: ', ';\n  a,\n  a:hover {\n    color: white;\n  }\n'], ['\n  position: absolute;\n  padding: 0 10px;\n  right: 100px;\n  top: 0px;\n  height: 40px;\n  font-size: 25px;\n  font-weight: 200;\n  lineheight: 40px;\n  background: ', ';\n  a,\n  a:hover {\n    color: white;\n  }\n']),
    _templateObject3 = _taggedTemplateLiteral(['\n  text-decoration: none;\n  border-bottom: 2px solid ', ';\n  color: black;\n\n  &:hover {\n    color: ', ';\n    text-decoration: none;\n  }\n'], ['\n  text-decoration: none;\n  border-bottom: 2px solid ', ';\n  color: black;\n\n  &:hover {\n    color: ', ';\n    text-decoration: none;\n  }\n']),
    _templateObject4 = _taggedTemplateLiteral(['\n  margin-top: 20px;\n  margin-bottom: 10px;\n  font-size: 14px;\n  color: ', ';\n'], ['\n  margin-top: 20px;\n  margin-bottom: 10px;\n  font-size: 14px;\n  color: ', ';\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var FormRow = exports.FormRow = _styledComponents2.default.div(_templateObject);

var CheckBoxStyles = exports.CheckBoxStyles = { display: 'inline-block', width: 200 };

var NewButton = exports.NewButton = _styledComponents2.default.div(_templateObject2, function (props) {
  return props.theme.primary1Color;
});

NewButton.defaultProps = {
  theme: {
    primaryColor: 'red'
  }
};

var Link = exports.Link = _styledComponents2.default.a(_templateObject3, function (props) {
  return props.theme.primary1Color;
}, function (props) {
  return props.theme.primary1Color;
});

Link.defaultProps = {
  theme: {
    primaryColor: 'red'
  }
};

var Label = exports.Label = _styledComponents2.default.div(_templateObject4, function (props) {
  return props.theme.labelColor;
});

Label.defaultProps = {
  theme: {
    primaryColor: 'red'
  }
};