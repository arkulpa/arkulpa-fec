"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _templateObject = _taggedTemplateLiteral(["\n  align-items: flex-end;\n  background-color: ", ";\n  border-radius: ", ";\n  border: solid 1px transparent;\n  color: ", ";\n  cursor: pointer;\n  display: flex;\n  font-size: ", ";\n  justify-content: center;\n  padding: ", ";\n  outline: none;\n  transition: all 200ms ease;\n\n  &:hover,\n  &:focus {\n    background-color: ", ";\n    box-shadow: 0 8px 8px 0 ", ";\n  }\n\n  ", ";\n"], ["\n  align-items: flex-end;\n  background-color: ", ";\n  border-radius: ", ";\n  border: solid 1px transparent;\n  color: ", ";\n  cursor: pointer;\n  display: flex;\n  font-size: ", ";\n  justify-content: center;\n  padding: ", ";\n  outline: none;\n  transition: all 200ms ease;\n\n  &:hover,\n  &:focus {\n    background-color: ", ";\n    box-shadow: 0 8px 8px 0 ", ";\n  }\n\n  ", ";\n"]);

var _styledComponents = require("styled-components");

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _polished = require("polished");

var _styledComponentsModifiers = require("styled-components-modifiers");

var _Icon = require("./Icon");

var _Icon2 = _interopRequireDefault(_Icon);

var _Text = require("./Text");

var _Text2 = _interopRequireDefault(_Text);

var _modifiers = require("../../modifiers");

var _gridScale = require("../../utils/gridScale");

var _gridScale2 = _interopRequireDefault(_gridScale);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var customModifiers = {
  fullWidth: function fullWidth() {
    return "flex: 1";
  }
};

var modifierConfig = _extends({}, _modifiers.bgColors, _modifiers.hoverBgColors, customModifiers);

var Button = _styledComponents2.default.button(_templateObject, function (props) {
  return props.theme.colors.chrome500;
}, function (props) {
  return props.theme.dimensions.borderRadius;
}, function (props) {
  return props.theme.colors.chrome000;
}, function (props) {
  return (0, _polished.rem)(props.theme.dimensionsFontSize);
}, function (props) {
  return (0, _gridScale2.default)(props, 1) + " " + (0, _gridScale2.default)(props, 3);
}, function (props) {
  return (0, _polished.lighten)(0.05, props.theme.colors.chrome500);
}, function (props) {
  return props.theme.colors.shadow;
}, (0, _styledComponentsModifiers.applyStyleModifiers)(modifierConfig));

Button.Icon = _Icon2.default;
Button.Text = _Text2.default;

/** @component */
exports.default = Button;