'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _templateObject = _taggedTemplateLiteral(['\n  font-size: 24px;\n  ', '\n'], ['\n  font-size: 24px;\n  ', '\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _polished = require('polished');

var _styledComponentsModifiers = require('styled-components-modifiers');

var _elements = require('../../elements');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var modifierConfig = {
  left: function left(props) {
    return '\n    padding-right: ' + (0, _polished.rem)(props.theme.dimensions.baseGrid) + ';\n  ';
  },
  right: function right(props) {
    return '\n    padding-left: ' + (0, _polished.rem)(props.theme.dimensions.baseGrid) + ';\n  ';
  }
};

var Icon = (0, _styledComponents2.default)(_elements.Icon)(_templateObject, (0, _styledComponentsModifiers.applyStyleModifiers)(modifierConfig));

exports.default = Icon;