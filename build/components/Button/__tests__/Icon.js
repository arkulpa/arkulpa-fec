'use strict';

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Icon = require('../Icon');

var _Icon2 = _interopRequireDefault(_Icon);

require('jest-styled-components');

var _helpers = require('__tests__/helpers');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

describe('Icon Element', function () {
  it('renders correctly', function () {
    var tree = (0, _helpers.renderWithTheme)(_react2.default.createElement(_Icon2.default, { name: 'home' })).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('adds the left modifier', function () {
    var tree = (0, _helpers.renderWithTheme)(_react2.default.createElement(_Icon2.default, {
      modifiers: ['left'],
      name: 'home'
    })).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('adds the right modifier', function () {
    var tree = (0, _helpers.renderWithTheme)(_react2.default.createElement(_Icon2.default, {
      modifiers: ['right'],
      name: 'home'
    })).toJSON();
    expect(tree).toMatchSnapshot();
  });
});