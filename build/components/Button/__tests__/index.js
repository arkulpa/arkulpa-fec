'use strict';

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _index = require('../index');

var _index2 = _interopRequireDefault(_index);

require('jest-styled-components');

var _helpers = require('__tests__/helpers');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

describe('Button Block', function () {
  it('renders correctly', function () {
    var tree = (0, _helpers.renderWithTheme)(_react2.default.createElement(
      _index2.default,
      null,
      _react2.default.createElement(_index2.default.Icon, {
        modifiers: ['left', 'chrome000'],
        name: 'home'
      }),
      _react2.default.createElement(
        _index2.default.Text,
        null,
        'action'
      )
    )).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('applies a bgColor modifier', function () {
    var tree = (0, _helpers.renderWithTheme)(_react2.default.createElement(
      _index2.default,
      { modifiers: ['bgPink'] },
      _react2.default.createElement(_index2.default.Icon, {
        modifiers: ['left', 'chrome000'],
        name: 'home'
      }),
      _react2.default.createElement(
        _index2.default.Text,
        null,
        'action'
      )
    )).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('applies a hoverBgColor modifier', function () {
    var tree = (0, _helpers.renderWithTheme)(_react2.default.createElement(
      _index2.default,
      { modifiers: ['hoverBgGreen'] },
      _react2.default.createElement(_index2.default.Icon, {
        modifiers: ['left', 'chrome000'],
        name: 'home'
      }),
      _react2.default.createElement(
        _index2.default.Text,
        null,
        'action'
      )
    )).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('applies the fullWidth modifier', function () {
    var tree = (0, _helpers.renderWithTheme)(_react2.default.createElement(
      _index2.default,
      { modifiers: ['fullWidth'] },
      _react2.default.createElement(_index2.default.Icon, {
        modifiers: ['left', 'chrome000'],
        name: 'home'
      }),
      _react2.default.createElement(
        _index2.default.Text,
        null,
        'action'
      )
    )).toJSON();
    expect(tree).toMatchSnapshot();
  });
});