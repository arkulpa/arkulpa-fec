'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _templateObject = _taggedTemplateLiteral(['\n  position: fixed;\n  top: 0;\n  left: 0;\n  right: 0;\n  border-bottom: 3px solid white;\n  background: ', ';\n  z-index: 1000;\n  color: white;\n  padding: 20px;\n  padding-bottom: 40px;\n  font-size: 20px;\n'], ['\n  position: fixed;\n  top: 0;\n  left: 0;\n  right: 0;\n  border-bottom: 3px solid white;\n  background: ', ';\n  z-index: 1000;\n  color: white;\n  padding: 20px;\n  padding-bottom: 40px;\n  font-size: 20px;\n']);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var ErrorContainer = _styledComponents2.default.div(_templateObject, function (props) {

  console.log(props);
  return props.theme.primary1Color;
});

ErrorContainer.defaultProps = {
  theme: {
    primary1Color: 'red'
  }
};

var ErrorMessage = function (_Component) {
  _inherits(ErrorMessage, _Component);

  function ErrorMessage() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, ErrorMessage);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = ErrorMessage.__proto__ || Object.getPrototypeOf(ErrorMessage)).call.apply(_ref, [this].concat(args))), _this), _this.clearTimeout = function () {
      return _this.timeout && clearTimeout(_this.timeout);
    }, _this.timeout = undefined, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(ErrorMessage, [{
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(newProps) {
      var _this2 = this;

      if (newProps.errors && newProps.errors.length > 0) {
        this.clearTimeout();
        this.timeout = setTimeout(function () {
          _this2.props.clearErrors();
        }, 4000);
      }
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      this.clearTimeout();
    }
  }, {
    key: 'render',
    value: function render() {
      var errors = this.props.errors;

      if (!errors || errors.length === 0) {
        return null;
      }
      return _react2.default.createElement(
        ErrorContainer,
        null,
        _react2.default.createElement(
          'h2',
          null,
          'Fehler:'
        ),
        errors.map(function (error, i) {
          var err = error;
          if (error.message) {
            err = error.message;
          }
          return _react2.default.createElement(
            'div',
            { key: 'error_' + i },
            err
          );
        })
      );
    }
  }]);

  return ErrorMessage;
}(_react.Component);

ErrorMessage.state = {
  timeout: false
};
ErrorMessage.propTypes = {
  clearErrors: _propTypes2.default.func.isRequired,
  errors: _propTypes2.default.array
};
exports.default = ErrorMessage;