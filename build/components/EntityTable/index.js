'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Td = exports.Th = exports.Tr = exports.Thead = exports.Table = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _templateObject = _taggedTemplateLiteral([''], ['']),
    _templateObject2 = _taggedTemplateLiteral(['\n  tr {\n    border: none !important;\n  }\n'], ['\n  tr {\n    border: none !important;\n  }\n']),
    _templateObject3 = _taggedTemplateLiteral(['\n  ', ' border-top: 1px solid lightgrey;\n  &:first-child {\n    border-top: 2px solid lightgrey;\n  }\n'], ['\n  ', ' border-top: 1px solid lightgrey;\n  &:first-child {\n    border-top: 2px solid lightgrey;\n  }\n']),
    _templateObject4 = _taggedTemplateLiteral(['\n  width: ', ';\n  color: black;\n  font-weight: normal;\n  padding: 5px;\n  border-top: 0;\n  text-align: left;\n'], ['\n  width: ', ';\n  color: black;\n  font-weight: normal;\n  padding: 5px;\n  border-top: 0;\n  text-align: left;\n']),
    _templateObject5 = _taggedTemplateLiteral(['\n  padding: 10px;\n  color: black;\n'], ['\n  padding: 10px;\n  color: black;\n']),
    _templateObject6 = _taggedTemplateLiteral(['\n  margin-top: -20px;\n  text-align: right;\n\n  .pagination li > a {\n    color: ', ' !important;\n  }\n\n  .pagination .active a,\n  .pagination .active a:hover {\n    color: #fff !important;\n    background-color: ', ' !important;\n    border-color: ', ' !important;\n  }\n'], ['\n  margin-top: -20px;\n  text-align: right;\n\n  .pagination li > a {\n    color: ', ' !important;\n  }\n\n  .pagination .active a,\n  .pagination .active a:hover {\n    color: #fff !important;\n    background-color: ', ' !important;\n    border-color: ', ' !important;\n  }\n']),
    _templateObject7 = _taggedTemplateLiteral(['\n  margin-bottom: 20px;\n  float: right;\n  width: 300px;\n'], ['\n  margin-bottom: 20px;\n  float: right;\n  width: 300px;\n']);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _TextField = require('material-ui/TextField');

var _TextField2 = _interopRequireDefault(_TextField);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _reactTooltip = require('react-tooltip');

var _reactTooltip2 = _interopRequireDefault(_reactTooltip);

var _reactJsPagination = require('react-js-pagination');

var _reactJsPagination2 = _interopRequireDefault(_reactJsPagination);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

// import Pagination from './Pagination';


var Table = exports.Table = _styledComponents2.default.table(_templateObject);
var Thead = exports.Thead = _styledComponents2.default.thead(_templateObject2);

var Tr = exports.Tr = _styledComponents2.default.tr(_templateObject3, function (_ref) {
  var selectable = _ref.selectable;
  return selectable && '\n        cursor: pointer;\n        &:hover {\n            background-color: lightgrey;\n        }\n    ';
});

var Th = exports.Th = _styledComponents2.default.th(_templateObject4, function (props) {
  return props.customWidth;
});

var Td = exports.Td = _styledComponents2.default.td(_templateObject5);
var PaginationHolder = _styledComponents2.default.div(_templateObject6, function (props) {
  return props.theme.primary1Color;
}, function (props) {
  return props.theme.primary1Color;
}, function (props) {
  return props.theme.primary1Color;
});

var SearchHolder = _styledComponents2.default.div(_templateObject7);

var EntityTable = function (_Component) {
  _inherits(EntityTable, _Component);

  function EntityTable(props) {
    _classCallCheck(this, EntityTable);

    var _this = _possibleConstructorReturn(this, (EntityTable.__proto__ || Object.getPrototypeOf(EntityTable)).call(this, props));

    _this._filterRows = function (filterVal, rows) {
      if (!rows) {
        rows = [];
      }
      var regex = new RegExp(filterVal, 'i');
      var filterString = _this.props.tableConfig.filterString;

      var filtered = rows.filter(function (entity) {
        var entityText = filterString ? filterString(entity) : Object.values(entity).toString();
        return entityText.search(regex) > -1;
      });
      _this.setState({ filteredRows: filtered, filterVal: filterVal });
    };

    _this._filter = function (event) {
      event.preventDefault();
      _this.setState({ currentPage: 1 });
      _this._filterRows(event.target.value, _this.props.data);
    };

    _this._updateRows = function (data) {
      _this._filterRows(_this.state.filterVal, data);
    };

    _this._paginationChange = function (page) {
      _this.setState({ currentPage: page });
    };

    _this.state = {
      currentPage: 1,
      filterVal: '',
      filteredRows: []
    };
    return _this;
  }

  _createClass(EntityTable, [{
    key: 'componentWillMount',
    value: function componentWillMount() {
      this._updateRows(this.props.data);
    }
  }, {
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(newProps) {
      this._updateRows(newProps.data);
    }
  }, {
    key: 'render',
    value: function render() {
      var _props = this.props,
          tableConfig = _props.tableConfig,
          totalRows = _props.totalRows,
          _props$searchEnabled = _props.searchEnabled,
          searchEnabled = _props$searchEnabled === undefined ? true : _props$searchEnabled,
          onRowClicked = _props.onRowClicked;

      var nothingFound = void 0;
      var pagination = void 0;
      var paginationRows = totalRows;
      if (this.state.totalRows === null) {
        paginationRows = this.state.filteredRows.length;
      }

      if (paginationRows > tableConfig.maxRows) {
        pagination = _react2.default.createElement(
          PaginationHolder,
          null,
          _react2.default.createElement(_reactJsPagination2.default, {
            activePage: this.state.currentPage,
            itemsCountPerPage: tableConfig.maxRows,
            onChange: this._paginationChange,
            pageRangeDisplayed: 5,
            totalItemsCount: paginationRows
          })
        );
      }

      var search = void 0;
      if (searchEnabled) {
        search = _react2.default.createElement(
          SearchHolder,
          null,
          _react2.default.createElement(_TextField2.default, {
            name: 'search',
            onChange: this._filter,
            placeholder: 'Suchen',
            type: 'text',
            value: this.state.filterVal
          })
        );
      }

      var page = this.state.currentPage;

      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(_reactTooltip2.default, null),
        search,
        _react2.default.createElement(
          Table,
          { style: { margin: '30px 0', width: '100%' } },
          _react2.default.createElement(
            Thead,
            null,
            _react2.default.createElement(
              Tr,
              null,
              tableConfig.cols.map(function (conf, idx) {
                if (!conf.header || conf.header === '') {
                  return _react2.default.createElement(
                    Th,
                    {
                      customWidth: conf.width,
                      key: idx
                    },
                    '\xA0'
                  );
                } else {
                  return _react2.default.createElement(
                    Th,
                    {
                      customWidth: conf.width,
                      key: idx
                    },
                    conf.header
                  );
                }
              })
            )
          ),
          _react2.default.createElement(
            'tbody',
            null,
            nothingFound,
            this.state.filteredRows.map(function (item, idx) {
              if (idx >= (page - 1) * tableConfig.maxRows && idx < page * tableConfig.maxRows) {
                return _react2.default.createElement(
                  Tr,
                  {
                    key: idx,
                    onClick: function onClick() {
                      return onRowClicked && onRowClicked(item);
                    },
                    selectable: !!onRowClicked
                  },
                  tableConfig.cols.map(function (conf, colIdx) {
                    var value = void 0;
                    if (conf.valueKey === '*') {
                      value = item;
                    } else {
                      value = item[conf.valueKey];
                    }

                    if (conf.tooltip) {
                      return _react2.default.createElement(
                        Td,
                        {
                          hasMore: conf.hasMore,
                          key: idx + '_' + colIdx
                        },
                        _react2.default.createElement(
                          'p',
                          { 'data-tip': conf.tooltip },
                          conf.method(value, item)
                        )
                      );
                    }
                    return _react2.default.createElement(
                      Td,
                      {
                        hasMore: conf.hasMore,
                        key: idx + '_' + colIdx
                      },
                      conf.method(value, item)
                    );
                  })
                );
              }
              return null;
            })
          )
        ),
        pagination
      );
    }
  }]);

  return EntityTable;
}(_react.Component);

EntityTable.propTypes = {
  data: _propTypes2.default.array,
  onRowClicked: _propTypes2.default.func,
  searchEnabled: _propTypes2.default.bool,
  tableConfig: _propTypes2.default.object.isRequired
};

exports.default = EntityTable;