'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _Dialog = require('material-ui/Dialog');

var _Dialog2 = _interopRequireDefault(_Dialog);

var _FlatButton = require('material-ui/FlatButton');

var _FlatButton2 = _interopRequireDefault(_FlatButton);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

var DeleteModal = function DeleteModal(_ref) {
  var onCancel = _ref.onCancel,
      onConfirm = _ref.onConfirm,
      props = _objectWithoutProperties(_ref, ['onCancel', 'onConfirm']);

  return _react2.default.createElement(
    _Dialog2.default,
    _extends({
      modal: true,
      title: 'L\xF6schen'
    }, props, {
      actions: [_react2.default.createElement(_FlatButton2.default, {
        key: 'cancel',
        label: 'Abbrechen',
        onTouchTap: onCancel
      }), _react2.default.createElement(_FlatButton2.default, {
        key: 'okd',
        label: 'Ja',
        onTouchTap: onConfirm,
        primary: true
      })]
    }),
    'Der Eintrag wird gel\xF6scht. Sind sie sich da wirklich sicher?'
  );
};

DeleteModal.propTypes = {
  onCancel: _propTypes2.default.func.isRequired,
  onConfirm: _propTypes2.default.func.isRequired
};

exports.default = DeleteModal;