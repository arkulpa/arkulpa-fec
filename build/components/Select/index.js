'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _materialUi = require('material-ui');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Select = function Select(_ref) {
  var label = _ref.label,
      selectedItem = _ref.selectedItem,
      items = _ref.items,
      changeMethod = _ref.changeMethod;
  return _react2.default.createElement(
    _materialUi.SelectField,
    {
      floatingLabelText: label,
      fullWidth: true,
      onChange: function onChange(_, __, value) {
        return changeMethod(value);
      },
      value: selectedItem
    },
    items.map(function (item, idx) {
      return _react2.default.createElement(_materialUi.MenuItem, {
        key: idx,
        primaryText: item.name,
        value: item._id
      });
    })
  );
};

Select.propTypes = {
  changeMethod: _propTypes2.default.func.isRequired,
  items: _propTypes2.default.array.isRequired,
  label: _propTypes2.default.string.isRequired,
  selectedItem: _propTypes2.default.object
};
exports.default = Select;