import React from 'react';
import PropTypes from 'prop-types';
import { MenuItem, SelectField } from 'material-ui';

const Select = ({ label, selectedItem, items, changeMethod }) => (
  <SelectField
    floatingLabelText={label}
    fullWidth
    onChange={(_, __, value) => changeMethod(value)}
    value={selectedItem}
  >
    {items.map((item, idx) => (<MenuItem
      key={idx}
      primaryText={item.name}
      value={item._id}
    />))}
  </SelectField>
);

Select.propTypes = {
  changeMethod: PropTypes.func.isRequired,
  items: PropTypes.array.isRequired,
  label: PropTypes.string.isRequired,
  selectedItem: PropTypes.object,
};
export default Select;
