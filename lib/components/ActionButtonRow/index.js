import styled from 'styled-components';

const ActionButtonRow = styled.div`
  border-top: 1px solid lightgrey;
  margin: 60px -40px 0;
  padding: 10px 20px 0;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;
export default ActionButtonRow;
