import ReactSidebar from 'react-sidebar';
import styled from 'styled-components';
import React from 'react';
import PropTypes from 'prop-types';


const LogoWrapper = styled.div`
  padding: 61px;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  height: 160px;
`;

export const sidebarStyles = {
  root: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    overflow: 'hidden',
  },
  sidebar: {
    zIndex: 2500,
    position: 'absolute',
    top: 0,
    bottom: 0,
    transition: 'transform .3s ease-out',
    WebkitTransition: '-webkit-transform .3s ease-out',
    willChange: 'transform',
    overflowY: 'auto',
    background: '#454849',
    padding: 40,
    minWidth: 250,
  },
  content: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    overflowY: 'scroll',
    WebkitOverflowScrolling: 'touch',
    transition: 'left .3s ease-out, right .3s ease-out',
  },
  overlay: {
    zIndex: 2,
    position: 'fixed',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    opacity: 0,
    visibility: 'hidden',
    transition: 'opacity .3s ease-out, visibility .3s ease-out',
    backgroundColor: 'rgba(0,0,0,.3)',
  },
  dragHandle: {
    zIndex: 1,
    position: 'fixed',
    top: 0,
    bottom: 0,
  },
};

export const SidebarNaviItem = styled.div`
  line-height: 50px;
  font-size: 25px;
  font-weight: 200;
  cursor: pointer;
  a,
  a:hover {
    color: ${props => props.theme.menuColor};
    text-decoration: none;
  }
  a:active,
  a:focus {
    color: ${props => props.theme.menuColor};
  }
`;

export const SidebarLogout = styled(SidebarNaviItem)`
  margin-top: 80px;
  color: ${props => props.theme.menuColor};
`;

export const SidebarNexGroupNavItem = styled(SidebarNaviItem)`
  margin-top: 60px;
`;

export const SidebarWrapper = styled.div`
  padding-top: 150px;
`;

export const SidebarClose = styled.div`
  position: absolute;
  bottom: 10px;
  left: 10px;
  text-align: center;
  right: 10px;
  font-size: 14px;
  color: white;
  cursor: pointer;
`;

export const SidebarToggle = styled.div`
  position: absolute;
  left: 20px;
  top: 20px;
  width: 40px;
  height: 40px;
  background:  ${props => props.theme.primary1Color};
  padding: 3px 3px 3px 6px;
  text-align: center;
  color: ${props => props.theme.accent1Color};
  cursor: pointer;
  z-index: 500;
  overflow: hidden;
`;

export class Sidebar extends React.Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    logo: PropTypes.string.isRequired,
    sidebarContent: PropTypes.node.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = { sidebarOpen: false };
  }

  handleOpenSidebar = open => {
    this.setState({ sidebarOpen: open });
    if (open) {
      document.addEventListener('keydown', this.handleESC, false);
    } else {
      document.removeEventListener('keydown', this.handleESC, false);
    }
  };

  handleESC = event => {
    if (event.keyCode === 27) {
      this.handleOpenSidebar(false);
    }
  };

  render() {
    const { children, logo, sidebarContent } = this.props;
    return (
      <ReactSidebar
        onSetOpen={this.handleOpenSidebar}
        open={this.state.sidebarOpen}
        sidebar={
          <div>
            <LogoWrapper>
              <img
                alt="Logo"
                src={logo}
                width="128"
              />
            </LogoWrapper>
            <div onClick={() => this.handleOpenSidebar(false)}>{sidebarContent}</div>
            <SidebarClose onClick={() => this.handleOpenSidebar(false)}>close (ESC)</SidebarClose>
          </div>
        }
        styles={sidebarStyles}
      >
        <SidebarToggle onClick={() => this.handleOpenSidebar(true)}>
          <img
            alt="Logo"
            height="34"
            src={logo}
            width="120"
          />
        </SidebarToggle>
        {children}
      </ReactSidebar>
    );
  }
}
