import styled from 'styled-components';

import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import React, { Component } from 'react';
import TextField from 'material-ui/TextField';

import ActionButtonRow  from '../ActionButtonRow';
import { Centered500 } from '../Container';
import { FormRow } from '../FormElements';
import ErrorMessage from '../ErrorMessage';
import convertErrors from '../../utils/errorHelper';
import filterOutNotDefinedValues from '../../utils/filterOutNotDefinedValues';

const LogoContainer = styled.div`
  padding: 20px;
  margin: 0 -15px 20px -15px;
  text-align: center;
  background: lightgrey;
`;

const StyledActionButtonRow = styled(ActionButtonRow)`
  margin: 20px -15px 0px;
  padding: 10px 20px;
`;

class Login extends Component {
  constructor() {
    super();
    this.state = { email: '', password: '', forgotPasswordSending: false, showForgotPasswordSuccessDialog: false };
  }

  login = () =>
    this.props
      .login(filterOutNotDefinedValues(this.state))
      .then(() => {
        this.props.rerender();
        this.props.history.replace('/');
      })
      .catch(rawErrors => {
        let errors = convertErrors(rawErrors);
        this.setState({ errors });
        console.log('there was an error sending the query', errors);
      });

  _renderForgotPasswordSuccessDialog = () => (
    <Dialog
      actions={[
        <FlatButton
          label="Ok"
          onTouchTap={() => this.setState({ showForgotPasswordSuccessDialog: false })}
          primary
        />,
      ]}
      modal
      open={this.state.showForgotPasswordSuccessDialog}
      title="Email für das Zurücksetzen versendet"
    >
      <div>
        Ihre Anfrage, um das Passwort zu ändern, wurde versendet.<br />
        Sie erhalten bald eine E-Mail mit dem benötigten Link.
      </div>
    </Dialog>
  );

  render() {
    const { logo, showRegister, showForgotPassword } = this.props.options;
    return (
      <div>
        <ErrorMessage
          clearErrors={() => this.setState({ errors: [] })}
          errors={this.state.errors}
        />
        <Centered500>
          <LogoContainer>{logo && <img
            alt="Logo"
            src={logo}
            width={128}
                                  />}</LogoContainer>
          <FormRow>
            <TextField
              autoFocus
              fullWidth
              hintText="Email"
              onChange={event => {
                this.setState({ email: event.target.value });
              }}
              value={this.state.email}
            />
          </FormRow>
          <FormRow>
            <TextField
              fullWidth
              hintText="Passwort"
              onChange={event => {
                this.setState({ password: event.target.value });
              }}
              onKeyPress={target => {
                if (target.charCode === 13) {
                  this.login();
                }
              }}
              type="password"
              value={this.state.password}
            />
          </FormRow>
          <StyledActionButtonRow>
            <div />
            <div>
              {showForgotPassword && (
                <FlatButton
                  disabled={this.state.forgotPasswordSending}
                  label="passwort vergessen"
                  onClick={() => {
                    let { email } = filterOutNotDefinedValues(this.state);
                    this.setState({ forgotPasswordSending: true });
                    this.props
                      .forgotPassword({ variables: { email } })
                      .then(({ data: { forgotPassword: success } }) =>
                        this.setState({ showForgotPasswordSuccessDialog: success, forgotPasswordSending: false })
                      )
                      .catch(rawErrors => {
                        this.setState({ forgotPasswordSending: false });
                        let errors = convertErrors(rawErrors);
                        this.setState({ errors });
                        console.log('there was an error sending the forgot password mutation', errors);
                      });
                  }}
                />
              )}
              {showRegister && <FlatButton
                label="registrieren"
                onClick={() => this.props.history.push('/register')}
                               />}
              <FlatButton
                label="anmelden"
                onClick={this.login}
                primary
              />
            </div>
          </StyledActionButtonRow>
        </Centered500>

        {showForgotPassword && this._renderForgotPasswordSuccessDialog()}
      </div>
    );
  }
}

Login.defaultProps = {
  options: {},
};

export default Login;
