import styled from 'styled-components';
export const INFO_WARNING = '#FEEFB3';
export const INFO_ERROR = '#FFD2D2';
export const INFO_SUCCESS = '#DFF2BF';

export const InfoText = styled.div`
  font-size: 20px;
  margin: 20px 0;
  padding: 20px 15px;
  font-weight: light;
  background: ${props => (props.type ? props.type : INFO_WARNING)};
`;
