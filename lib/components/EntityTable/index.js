import PropTypes from 'prop-types';
import React, { Component } from 'react';

import TextField from 'material-ui/TextField';

// import Pagination from './Pagination';
import styled from 'styled-components';
import ReactTooltip from 'react-tooltip';
import Pagination from 'react-js-pagination';

export const Table = styled.table``;
export const Thead = styled.thead`
  tr {
    border: none !important;
  }
`;

export const Tr = styled.tr`
  ${({ selectable }) =>
    selectable &&
    `
        cursor: pointer;
        &:hover {
            background-color: lightgrey;
        }
    `} border-top: 1px solid lightgrey;
  &:first-child {
    border-top: 2px solid lightgrey;
  }
`;

export const Th = styled.th`
  width: ${props => props.customWidth};
  color: black;
  font-weight: normal;
  padding: 5px;
  border-top: 0;
  text-align: left;
`;

export const Td = styled.td`
  padding: 10px;
  color: black;
`;
const PaginationHolder = styled.div`
  margin-top: -20px;
  text-align: right;

  .pagination li > a {
    color: ${props => props.theme.primary1Color} !important;
  }

  .pagination .active a,
  .pagination .active a:hover {
    color: #fff !important;
    background-color: ${props=> props.theme.primary1Color} !important;
    border-color: ${props => props.theme.primary1Color} !important;
  }
`;

const SearchHolder = styled.div`
  margin-bottom: 20px;
  float: right;
  width: 300px;
`;

class EntityTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPage: 1,
      filterVal: '',
      filteredRows: [],
    };
  }

  componentWillMount() {
    this._updateRows(this.props.data);
  }

  componentWillReceiveProps(newProps) {
    this._updateRows(newProps.data);
  }

  _filterRows = (filterVal, rows) => {
    if (!rows) {
      rows = [];
    }
    let regex = new RegExp(filterVal, 'i');
    let { filterString } = this.props.tableConfig;
    let filtered = rows.filter(entity => {
      let entityText = filterString ? filterString(entity) : Object.values(entity).toString();
      return entityText.search(regex) > -1;
    });
    this.setState({ filteredRows: filtered, filterVal: filterVal });
  };

  _filter = event => {
    event.preventDefault();
    this.setState({ currentPage: 1 });
    this._filterRows(event.target.value, this.props.data);
  };

  _updateRows = data => {
    this._filterRows(this.state.filterVal, data);
  };

  _paginationChange = page => {
    this.setState({ currentPage: page });
  };

  render() {
    let { tableConfig, totalRows, searchEnabled = true, onRowClicked } = this.props;
    let nothingFound;
    let pagination;
    let paginationRows = totalRows;
    if (this.state.totalRows === null) {
      paginationRows = this.state.filteredRows.length;
    }

    if (paginationRows > tableConfig.maxRows) {
      pagination = (
        <PaginationHolder>
          <Pagination
            activePage={this.state.currentPage}
            itemsCountPerPage={tableConfig.maxRows}
            onChange={this._paginationChange}
            pageRangeDisplayed={5}
            totalItemsCount={paginationRows}
          />
        </PaginationHolder>
      );
    }

    let search;
    if (searchEnabled) {
      search = (
        <SearchHolder>
          <TextField
            name="search"
            onChange={this._filter}
            placeholder={'Suchen'}
            type="text"
            value={this.state.filterVal}
          />
        </SearchHolder>
      );
    }

    let page = this.state.currentPage;

    return (
      <div>
        <ReactTooltip />
        {search}
        <Table style={{ margin: '30px 0', width: '100%' }}>
          <Thead>
            <Tr>
              {tableConfig.cols.map((conf, idx) => {
                if (!conf.header || conf.header === '') {
                  return (
                    <Th
                      customWidth={conf.width}
                      key={idx}
                    >
                      &nbsp;
                    </Th>
                  );
                } else {
                  return (
                    <Th
                      customWidth={conf.width}
                      key={idx}
                    >
                      {conf.header}
                    </Th>
                  );
                }
              })}
            </Tr>
          </Thead>
          <tbody>
            {nothingFound}
            {this.state.filteredRows.map((item, idx) => {
              if (idx >= (page - 1) * tableConfig.maxRows && idx < page * tableConfig.maxRows) {
                return (
                  <Tr
                    key={idx}
                    onClick={() => onRowClicked && onRowClicked(item)}
                    selectable={!!onRowClicked}
                  >
                    {tableConfig.cols.map((conf, colIdx) => {
                      let value;
                      if (conf.valueKey === '*') {
                        value = item;
                      } else {
                        value = item[conf.valueKey];
                      }

                      if (conf.tooltip) {
                        return (
                          <Td
                            hasMore={conf.hasMore}
                            key={idx + '_' + colIdx}
                          >
                            <p data-tip={conf.tooltip}>{conf.method(value, item)}</p>
                          </Td>
                        );
                      }
                      return (
                        <Td
                          hasMore={conf.hasMore}
                          key={idx + '_' + colIdx}
                        >
                          {conf.method(value, item)}
                        </Td>
                      );
                    })}
                  </Tr>
                );
              }
              return null;
            })}
          </tbody>
        </Table>
        {pagination}
      </div>
    );
  }
}

EntityTable.propTypes = {
  data: PropTypes.array,
  onRowClicked: PropTypes.func,
  searchEnabled: PropTypes.bool,
  tableConfig: PropTypes.object.isRequired,
};

export default EntityTable;
