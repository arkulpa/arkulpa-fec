import styled from 'styled-components';

export const HeadlineH1 = styled.h1`
  font-size: 52px;
  font-weight: lighter;
  margin: 0.6em 0 0.1em;
`;
export const HeadlineH2 = styled.h2`
  font-size: 25px;
  font-weight: 200;
  margin: ${props => (props.margin ? props.margin : '2em 0 0.5em')};
`;
