// this is the primary export for Blocks
// All components are exported from lib/index.js
import ActionButtonRow from './ActionButtonRow';
import AuthRoute from './AuthRoute';
import * as Container from './Container';
import DeleteModal from './DeleteModal';
import EntityTable from './EntityTable';
import ErrorMessage from './ErrorMessage';
import * as FormElements from './FormElements';
import FullScreenWrapper from './FullscreenWrapper';
import * as InfoText from './InfoText';
import LoadingIndicator from './LoadingIndicator';
import Login from './Login';
import Select from './Select';
import Selector from './Selector';
import * as Sidebar from './Sidebar';
import SidebarToggle from './SidebarToggle';
import * as Text from './Text';

module.exports = {
  // Blocks will be exported here
  ActionButtonRow,
  ErrorMessage,
  Login,
  ...Container,
  ...FormElements,
  SidebarToggle,
  AuthRoute,
  ...Text,
  EntityTable,
  LoadingIndicator,
  ...Sidebar,
  Select,
  Selector,
  DeleteModal,
  ...InfoText,
  FullScreenWrapper
};
