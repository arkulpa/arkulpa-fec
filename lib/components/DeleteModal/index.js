import React from 'react';
import PropTypes from 'prop-types';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';

const DeleteModal = ({ onCancel, onConfirm, ...props }) => (
  <Dialog
    modal
    title="Löschen"
    {...props}
    actions={[
      <FlatButton
        key={'cancel'}
        label="Abbrechen"
        onTouchTap={onCancel}
      />,
      <FlatButton
        key={'okd'}
        label="Ja"
        onTouchTap={onConfirm}
        primary
      />,
    ]}
  >
    Der Eintrag wird gelöscht. Sind sie sich da wirklich sicher?
  </Dialog>
);

DeleteModal.propTypes = {
  onCancel: PropTypes.func.isRequired,
  onConfirm: PropTypes.func.isRequired,
};

export default DeleteModal;
