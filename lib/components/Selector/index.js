import styled from 'styled-components';
import PropTypes from 'prop-types';
import React from 'react';

const Option = styled.div`
  display: inline-block;
  border-radius: 5px;
  padding: 0px 10px;
  margin: 0 10px 10px 0;
  line-height: 30px;
  background-color: ${({ selected, theme }) => (selected ? theme.primary1Color : theme.accent1Color)};
  color: ${({ selected }) => (selected ? 'white' : 'black')};
  cursor: pointer;
`;

const Wrapper = styled.div`
  margin-bottom: -10px;
`;

const _isSelected = (item, selected) => selected.findIndex(({ _id }) => _id === item._id) !== -1;

const _toggle = (item, selected, updateMethod, multiSelect) => {
  if (!multiSelect) {
    updateMethod([item]);
    return;
  }
  let filtered = selected.filter(({ _id }) => _id !== item._id);
  if (filtered.length === selected.length) {
    filtered.push(item);
  }
  updateMethod(filtered);
};

const Selector = ({ selectedItems, items, changeMethod, multiselect, renderItem }) => {
  return (
    <Wrapper>
      {items.map(item => (
        <Option
          key={item._id}
          onClick={() => _toggle(item, selectedItems, changeMethod, multiselect)}
          selected={_isSelected(item, selectedItems)}
        >
          {renderItem(item)}
        </Option>
      ))}
    </Wrapper>
  );
};

Selector.propTypes = {
  changeMethod: PropTypes.func.isRequired,
  items: PropTypes.array.isRequired,
  multiselect: PropTypes.bool,
  renderItem: PropTypes.func.isRequired,
  selectedItems: PropTypes.array.isArray,
};

export default Selector;
