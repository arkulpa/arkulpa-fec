import { Route, Redirect } from 'react-router-dom';
import React, { Component } from 'react';

export default class AuthRoute extends Component {
  shouldComponentUpdate({ user, path }) {
    let oldUser = this.props.user;
    let newUserId = user && user._id;
    let oldUserId = oldUser && oldUser._id;
    return newUserId !== oldUserId || path !== this.props.path;
  }

  render() {
    let { component: Component, user, role, ...rest } = this.props;
    return (
      <Route
        {...rest}
        render={props => {
          let isAllowed = false;
          if (!role && user) {
            isAllowed = true;
          } else if (user) {
            isAllowed = user.role === role;
          }
          return isAllowed ? (
            <Component
              {...props}
              user={user}
            />
          ) : (
            <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
          );
        }}
      />
    );
  }
}
