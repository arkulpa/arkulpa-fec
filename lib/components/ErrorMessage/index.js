import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const ErrorContainer = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  border-bottom: 3px solid white;
  background: ${props => {
    
    console.log(props);
    return props.theme.primary1Color;}};
  z-index: 1000;
  color: white;
  padding: 20px;
  padding-bottom: 40px;
  font-size: 20px;
`;

ErrorContainer.defaultProps = {
  theme: {
    primary1Color: 'red',
  },
};

class ErrorMessage extends Component {
  static state = {
    timeout: false,
  };

  static propTypes = {
    clearErrors: PropTypes.func.isRequired,
    errors: PropTypes.array,
  };

  componentWillReceiveProps(newProps) {
    if (newProps.errors && newProps.errors.length > 0) {
      this.clearTimeout();
      this.timeout = setTimeout(() => {
        this.props.clearErrors();
      }, 4000);
    }
  }

  componentWillUnmount() {
    this.clearTimeout();
  }

  clearTimeout = () => this.timeout && clearTimeout(this.timeout);
  timeout = undefined;
  render() {
    const { errors } = this.props;
    if (!errors || errors.length === 0) {
      return null;
    }
    return (
      <ErrorContainer>
        <h2>Fehler:</h2>
        {errors.map((error, i) => {
          let err = error;
          if (error.message) {
            err = error.message;
          }
          return <div key={'error_' + i}>{err}</div>;
        })}
      </ErrorContainer>
    );
  }
}

export default ErrorMessage;
