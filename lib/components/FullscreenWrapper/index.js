import styled from 'styled-components';


const FullScreenWrapper = styled.div`
  width: 100%;
  heigth: 100%;
  z-index: 50;
  background: white;
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  overflow: scroll;
`;

export default FullScreenWrapper;
