import styled from 'styled-components';

export const FormRow = styled.div`
  padding: 10px 15px;
`;

export const CheckBoxStyles = { display: 'inline-block', width: 200 };

export const NewButton = styled.div`
  position: absolute;
  padding: 0 10px;
  right: 100px;
  top: 0px;
  height: 40px;
  font-size: 25px;
  font-weight: 200;
  lineheight: 40px;
  background: ${props => props.theme.primary1Color};
  a,
  a:hover {
    color: white;
  }
`;

NewButton.defaultProps = {
  theme: {
    primaryColor: 'red',
  },
};

export const Link = styled.a`
  text-decoration: none;
  border-bottom: 2px solid ${props => props.theme.primary1Color};
  color: black;

  &:hover {
    color: ${props => props.theme.primary1Color};
    text-decoration: none;
  }
`;

Link.defaultProps = {
  theme: {
    primaryColor: 'red',
  },
};

export const Label = styled.div`
  margin-top: 20px;
  margin-bottom: 10px;
  font-size: 14px;
  color: ${props => props.theme.labelColor};
`;

Label.defaultProps = {
  theme: {
    primaryColor: 'red',
  },
};
