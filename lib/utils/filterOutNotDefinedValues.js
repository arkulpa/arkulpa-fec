//get all defined properties (filters out null, undefined, ...)
export default object =>
  object &&
  Object.keys(object).reduce((defined, key) => {
    let value = object[key];
    if (value === false || value === 0 || (value && value !== '')) {defined[key] = value;}
    return defined;
  }, {});
