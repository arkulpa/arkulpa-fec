export default function(array, oldIndex, positionChange) {
  const value = array[oldIndex];
  if (oldIndex > -1) {
    var newIndex = oldIndex + positionChange;

    if (newIndex < 0) {
      newIndex = 0;
    } else if (newIndex >= array.length) {
      newIndex = array.length;
    }

    var arrayClone = array.slice();
    arrayClone.splice(oldIndex, 1);
    arrayClone.splice(newIndex, 0, value);
    return arrayClone;
  }
  return array;
}
