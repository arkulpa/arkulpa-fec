
const MISSING_FIELD = /Variable "\$(.*)" of required type .* was not provided\./;
const MISSING_FIELD_MONGOOSE = /.*validation failed: .*: Path `(.*)` is required\./;

export const MissingInputError = { message: 'Nicht alle notwendigen Informationen wurden ausgefüllt.' };
export const DetailMissingInputError = field => ({
  message: 'Nicht alle notwendigen Informationen wurden ausgefüllt.' + (field ? ` (${field})` : '')
});

export default ({ graphQLErrors, networkError }) => {
  let errorMessages = [];
  if (graphQLErrors) {
    errorMessages = graphQLErrors.reduce((result, { message, ...rest }, i, { length }) => {
      let missingFields = MISSING_FIELD.exec(message);
      if (!missingFields) {
        missingFields = MISSING_FIELD_MONGOOSE.exec(message);
      }
      if (missingFields) {
        let field = convertMissingField(missingFields[1]);
        result.push(DetailMissingInputError(field));
      } else {
        result.push({ message });
      }
      return result;
    }, errorMessages);
  }

  if (networkError) {
    errorMessages.push({ message: 'Leider ist ein Problem mit der Verbindung aufgetreten.' });
  }

  return errorMessages;
};

function convertMissingField(field) {
  switch (field) {
    case 'email':
      return 'Email';
    case 'phone':
      return 'Telefonnummer';
    case 'firstName':
      return 'Vorname';
    case 'lastName':
      return 'Nachname';
    case 'birthdayStr':
      return 'Geburtstag';
    case 'street':
      return 'Straße';
    case 'zipCode':
      return 'PLZ';
    case 'city':
      return 'Wohnort';
    case 'svNumber':
      return 'SV Nummer';
    case 'passportNumber':
      return 'Passnummer';
    case 'passportAgency':
      return 'Ausstellungsbehörde';
    case 'education':
      return 'Schulbildung';
    case 'job':
      return 'Beruf';
    case 'name':
      return 'Name';
    case 'locale':
      return 'Locale';
    case 'startDateStr':
      return 'Startzeitpunkt';
    case 'endDateStr':
      return 'Endzeitpunkt';
    case 'meetingPoint':
      return 'Treffpunkt';
    case 'password':
      return 'Passwort';
    case 'nationality':
      return 'Staatsbürgerschaft';
    case 'subject':
      return 'Betreff';
    case 'text':
      return 'Nachricht';
    default:
      return field;
  }
}
